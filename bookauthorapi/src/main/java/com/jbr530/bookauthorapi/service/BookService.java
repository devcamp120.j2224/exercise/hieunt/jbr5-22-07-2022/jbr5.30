package com.jbr530.bookauthorapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.jbr530.bookauthorapi.model.Book;


@Service
public class BookService {
    private static ArrayList<Book> allBooks = new ArrayList<Book>();
    static {
        Book book1 = new Book("Book1",
            AuthorService.getAuthorList1(), 35000, 9);
        Book book2 = new Book("Book2",
            AuthorService.getAuthorList2(), 45000, 7);
        Book book3 = new Book("Book3",
            AuthorService.getAuthorList3(), 60000, 11);
        allBooks.add(book1);
        allBooks.add(book2);
        allBooks.add(book3);
    }
    public static ArrayList<Book> getAllBooks() {
        return allBooks;
    }
    public static void setAllBooks(ArrayList<Book> allBooks) {
        BookService.allBooks = allBooks;
    }
}
