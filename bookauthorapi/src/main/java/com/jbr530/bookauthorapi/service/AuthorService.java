package com.jbr530.bookauthorapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.jbr530.bookauthorapi.model.Author;

@Service
public class AuthorService {
    private static ArrayList<Author> authorList1 = new ArrayList<>();
    private static ArrayList<Author> authorList2 = new ArrayList<>();
    private static ArrayList<Author> authorList3 = new ArrayList<>();
    private static ArrayList<Author> allAuthors = new ArrayList<>();
    static {
        Author author11 = new Author("Lan", "lan@gmail.com", 'f');
        Author author12 = new Author("Nam", "nam@gmail.com", 'm');
        Author author21 = new Author("Hoa", "hoa@gmail.com", 'f');
        Author author22 = new Author("Mai", "mai@gmail.com", 'f');
        Author author31 = new Author("Hương", "huong@gmail.com", 'f');
        Author author32 = new Author("Trung", "trung@gmail.com", 'm');
        authorList1.add(author11); authorList1.add(author12);
        authorList2.add(author21); authorList2.add(author22);
        authorList3.add(author31); authorList3.add(author32);
        for (Author bAuthor : authorList1) {
            allAuthors.add(bAuthor);
        }
        for (Author bAuthor : authorList2) {
            allAuthors.add(bAuthor);
        }
        for (Author bAuthor : authorList3) {
            allAuthors.add(bAuthor);
        }
    }
    public static ArrayList<Author> getAuthorList1() {
        return authorList1;
    }
    public static void setAuthorList1(ArrayList<Author> authorList1) {
        AuthorService.authorList1 = authorList1;
    }
    public static ArrayList<Author> getAuthorList2() {
        return authorList2;
    }
    public static void setAuthorList2(ArrayList<Author> authorList2) {
        AuthorService.authorList2 = authorList2;
    }
    public static ArrayList<Author> getAuthorList3() {
        return authorList3;
    }
    public static void setAuthorList3(ArrayList<Author> authorList3) {
        AuthorService.authorList3 = authorList3;
    }
    public static ArrayList<Author> getAllAuthors() {
        return allAuthors;
    }
}
