package com.jbr530.bookauthorapi.controller;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.*;

import com.jbr530.bookauthorapi.model.Book;
import com.jbr530.bookauthorapi.service.BookService;


@RestController
@CrossOrigin
public class BookController {
    @GetMapping("/books")
    public ArrayList<Book> getAllBooks() {
        ArrayList<Book> allBooks = BookService.getAllBooks();
        return allBooks;
    }

    @GetMapping("/book-quantity")
    public ArrayList<Book> getBooksByQuantityNumber(@RequestParam(required = true) int quantityNumber) {
        ArrayList<Book> allBooks = BookService.getAllBooks();
        ArrayList<Book> result = new ArrayList<>();
        for(Book bBook : allBooks) {
            if(bBook.getQty() >= quantityNumber) {
                result.add(bBook);
            }
        }
        return result;
    }
}
