package com.jbr530.bookauthorapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.jbr530.bookauthorapi.model.Author;
import com.jbr530.bookauthorapi.service.AuthorService;


@RestController
@CrossOrigin
public class AuthorController {
    @GetMapping("/author-info")
    public Author getAuthorByEmail(@RequestParam(required = true) String email) {
        ArrayList<Author> allAuthors = AuthorService.getAllAuthors();
        Author result = null;
        for(Author bAuthor : allAuthors) {
            if(bAuthor.getEmail().equals(email)) {
                result = bAuthor ;
            }
        }
        return result;
    }

    @GetMapping("/author-gender")
    public ArrayList<Author> getAuthorsByGender(@RequestParam(required = true) char gender) {
        ArrayList<Author> allAuthors = AuthorService.getAllAuthors();
        ArrayList<Author> result = new ArrayList<>();
        for(Author bAuthor : allAuthors) {
            if(bAuthor.getGender() == gender) {
                result.add(bAuthor) ;
            }
        }
        return result;
    }
}
